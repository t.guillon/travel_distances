import itertools
import json
import pathlib

capitals_folder = pathlib.Path('cities')

files = capitals_folder.glob('*.json')

continents = dict(
    (file.stem, json.load(file.open(encoding='utf-8')))
    for file in files
    )

cities = dict(
    (   continent,
        dict(
            (   country['properties']['capital'],
                country['geometry']['coordinates']
                )
            for country in countries
            )
        )
    for continent, countries in continents.items()
    )
    
# all cities
long_lat = dict(
    itertools.chain(*[cit_per_cont.items() for cit_per_cont in cities.values()])
    )

