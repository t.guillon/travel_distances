import numpy as np
import scipy.spatial as scispa
# from cities import long_lat
from get_capitals import long_lat

avg_earth_radius = 6371.e3

def gps_degrees_to_decimal(degrees, minutes, seconds):
    return degrees+minutes/60.+seconds/3600.
    

def haversine(long_lat_start, long_lat_end, radius=avg_earth_radius):
    """
    see https://en.wikipedia.org/wiki/Great-circle_distance
    """
    sin2_dlong, sin2_dlat = np.sin(
        (long_lat_end-long_lat_start)/2.
        ).T**2
    sin2_sumlat = np.sin(
        (long_lat_start+long_lat_end)/2.
        )[...,-1]**2
    dist = 2*radius*np.arcsin(
        np.sqrt(
            sin2_dlat
            + (1-sin2_dlat-sin2_sumlat)*sin2_dlong
            )
        )
    # 2*r*arcsin(sqrt(sin((phi2-phi1)/2.)**2 + cos(phi1)*cos(phi2)*sin((lambd2-lambd1)/2.)**2))*1.e-3
    return dist
    
def vincenty_simplified():
    return

angers = np.deg2rad([
    - gps_degrees_to_decimal(0,33,15),
    gps_degrees_to_decimal(47,28,25)
    ])
long_lat = dict(
    (city, coo) for city, coo in zip(
        long_lat,
        np.deg2rad(list(long_lat.values()))
        )
    )
some_cities = np.array(list(long_lat.values()))
dists = dict(zip(long_lat.keys(), haversine(angers, some_cities)))
print('Distances from angers')
for city, dist in sorted(dists.items(), key=lambda item: item[1]):
    print(f'{city}: {dist*1.e-3:.2f} km')
    